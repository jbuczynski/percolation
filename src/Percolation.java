 /**
      Percolation class  
   */
public class Percolation{
	private Boolean percolationArray[][];	
	private WeightedQuickUnionUF UF;
	private int N, top, bottom;
	 /**
	 create N-by-N grid, with all sites blocked 
	
	 @param   int dimension of grid.
	  */   
	public Percolation(int N) {
	    // create N-by-N grid, with all sites blocked	   
	    this.N=N;	    
	    this.percolationArray = new Boolean[N][N];
	    for(int i=0; i< N;i++)
		    for(int j=0; j< N;j++)
		        this.percolationArray[i][j] = false;
		
		this.UF = new WeightedQuickUnionUF((N*N)+2);
		this.top = N*N;
		this.bottom =(N*N)+1;
    
		for(int i = 0; i < N; i++) {
		    this.UF.union(index(0,i),top);
		    this.UF.union(index(N-1,i),bottom);
		}
	}	
	 /**
    open site (row i, column j) if it is not already 
   
    @param   grid row
    @param   grid column
     */   
	
	public void open(int i, int j) {
	    indexValidating(index(i-1,j-1));
	    if(isOpen(i, j)) return;
	    i--;
	    j--;	   
		percolationArray[i][j] = true;
		if(noExceptionIndexVal(i+1,j) && percolationArray[i+1][j])  UF.union(index(i+1,j),index(i,j));
		if(noExceptionIndexVal(i-1,j) && percolationArray[i-1][j])  UF.union(index(i-1,j),index(i,j));    
		if(noExceptionIndexVal(i,j+1) && percolationArray[i][j+1])  UF.union(index(i,j+1),index(i,j));
		if(noExceptionIndexVal(i,j-1) && percolationArray[i][j-1])  UF.union(index(i,j-1),index(i,j));    		
	}

    /**
    is site (row i, column j) open?
   
    @param   grid row
    @param   grid column
     */    
	public boolean isOpen(int i, int j) {
		
	    i = i-1;
        j = j-1;
		return percolationArray[i][j];
	}
	 /**
    check is site full
   
    @param   grid row
    @param   grid column
     */   
	public boolean isFull(int i, int j) {
		
	    i = i-1;
        j = j-1;
	    if(percolationArray[i][j] &&UF.connected(index(i,j), top)) return true;	   
	    else return false;
	}
	 /**
   check does the system percolate  
  
     */ 
	public boolean percolates() {
		
		return UF.connected(top, bottom);
	}
	
	private int index(int row,int column)
	{	  
	    return row * N + column;
	}
	
	private void indexValidating(int i)
	{
	    if ((i < 0 || i >= N*N)) throw new IndexOutOfBoundsException("row index i out of bounds");
	}
	
	private Boolean noExceptionIndexVal(int i, int j)
    {
        if ((i < 0 || i >= N) || (j < 0 || j >= N)) return false;
        else return true;
    }	
	
}

