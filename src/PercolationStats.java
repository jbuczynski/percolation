/**
 * Percolation stats
 * */
 
public class PercolationStats {
    private double[] oneProbeThresholds;
    private int T;
    private double N;
    private double nSquare;
   
    /**
     *  perform T independent computational experiments on an N-by-N grid
     * @param N -  int dimension of percolation grid(N*N)
     * @param T - int number of probes
     */
	   public PercolationStats(int N, int T)  
	   {
	       this.N=N;
	       this.nSquare = N*N;
	       this.T=T;
	      oneProbeThresholds = new double[T];
	      
	       for(int i = 0; i<T; i++)
	       {
	           double opened = 0;
	           Percolation perc = new Percolation(N);
	           while(!perc.percolates())
	           {
	               int k = StdRandom.uniform(1, N+1);
	               int l = StdRandom.uniform(1, N+1); 
	               if(!perc.isOpen(k, l))
	               {
	               perc.open(k, l);
	               opened++;
	               }
	           }
	           oneProbeThresholds[i]=opened/nSquare;
	       }
	   }
	   
	   /**
	    * sample mean of percolation threshold
	    * @return percolation threshold value, 
	    */
	   public double mean()                    
	   {
	       double sum = 0;
	       for(int i = 0; i<T; i++)
	           sum+=oneProbeThresholds[i];
	       
	       return sum/T;
	   }
	
	   /**
	    * 
	    * @return standard deviation of threshold
	    */
	   public double stddev()   
	   {
	       if(T<=1) return Double.NaN;
	       
	       double s,squareAverage=0;
	       for(int i = 0; i<T;i++ )	     
	           squareAverage += oneProbeThresholds[i] * oneProbeThresholds[i];
	       squareAverage = squareAverage/T;
	       s = T/(T-1)*(squareAverage - mean()*mean());
	       
	       return Math.sqrt(s);
	   }
	   
	
	   /**
	    * 
	    * @return returns lower bound of the 95% confidence interval
	    */
	   public double confidenceLo()     
	   {
	       return mean()-stddev();
	   }

	   /**
	    * 
	    * @return  returns upper bound of the 95% confidence interval
	    */
	   public double confidenceHi()      
	   {
	       return mean()+stddev();
	   }
	   
	  
	   // test client, described below
	   public static void main(String[] args) 
	   {
	       PercolationStats stats = new PercolationStats(200, 100);
	       
	       System.out.println("Mean percolation treshold = " + String.valueOf(stats.mean()));
	       System.out.println(" standard deviation of treshold = " + String.valueOf(stats.stddev()));
	       System.out.println(" lower bound of the 95% confidence interval = " + String.valueOf(stats.confidenceLo()));
	       System.out.println(" upper bound of the 95% confidence interval = " + String.valueOf(stats.confidenceHi()));
	   }
	}